const { assoc, map, isNil, prop, append, pipe, or, isEmpty, difference, length, equals, keys, curry, __ } = require('ramda');
const anydb = require('any-db');
const inmemory = require('./inmemory');
var db;

const async = require('async');
const log = (str) => console.log("DB: " + JSON.stringify(str));
const error = (str) => console.error("DB: [ERROR] " + JSON.stringify(str));

const HAS_NO_ATTRIBUTES = "Type has no attributes field";
const TYPE_MISSMATCH = "Type definition dosen't match added data";
const INMEMORY = "INMEMORY";

const generateError = curry((type, data) => ({
  type,
  data
}))

const attributes = prop("attributes");
const hasSameElements = pipe(difference, length, equals(0))

const sameType = (page, attribute) =>
      hasSameElements(attributes(page), keys(attribute));

const generator = (name, attributes) => { name, attributes };

const add = curry((type, data, cb) => {
  if (!sameType(type, data)) {
    const err = generateError(TYPE_MISSMATCH, type);
	  error(err);
	  return cb(err, null);
  }

  const composition = async.seq(db.add, db.relate(type));

  composition(data, cb);
});

const adds = curry((type, datas, cb) =>
		   async.series(map(add(type), datas), cb));

const parents = doc => prop('parents', db)(doc);
const children = doc => prop('children', db)(doc);

const initialize = connection => {
  // 'sqlite3://test.db'
  if (connection === INMEMORY) {
    db = inmemory;
  } else {
    db = anydb.createConnection(connection);
  }
};

module.exports = {
  initialize,
  generator,
  errors: {
    TYPE_MISSMATCH,
    HAS_NO_ATTRIBUTES,
    INMEMORY
  },
  sameType,
  add,
  adds,
  parents,
  children
}
