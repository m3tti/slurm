const db = require('../db');
const { values, head } = require('ramda');

const saveType = db.initializeType({ name: 'hans', attributes: ['name'] });

test('success initializeType', () => {
  const cb = (err, res) =>
        expect(res).toEqual(
          expect.objectContaining({
            id: expect.any(String),
            name: expect.stringContaining("hans"),
            attributes: expect.arrayContaining(['name'])
          }));
  
  saveType(cb);
});

test('success add', () => {
  const cb = (err, res) => {
    expect(res).toHaveProperty('relations');
    expect(res).toHaveProperty('documents');
    
    const relation = head(values(res.relations));
    const docs = values(res.documents);
    
    expect(docs.length).toBe(3);
    expect(relation.length).toBe(1);
  };

  saveType((err, type) => 
           db.add(type, { name: 'hans'}, cb));
});

test('error initializeType', () => {
  const data = { name: 'wurst' };
  const cb = (err) => {
    expect(err).toMatchObject({
      type: expect.stringContaining(db.errors.HAS_NO_ATTRIBUTES),
      data: expect.objectContaining(data)
    });
  };

  db.initializeType(data, cb);
});

test('error add', () => {
  let ty;
  const cb = (err) => expect(err).toMatchObject({
    type: expect.stringContaining(db.errors.TYPE_MISSMATCH),
    data: expect.objectContaining(ty)
  });
  
  saveType((err, type) => {
    ty = type;
    db.add(type, { notinscope: 'hans'}, cb);
  });
});
