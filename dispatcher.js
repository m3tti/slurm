const { parallel, asyncify } = require('async');
const { prop, curry, map, path, pipe, __, call, merge, assoc } = require('ramda');

var actions = {};

const action = prop('action');
const payload = prop('payload');
const res = prop('res');

const getFunction = act => 
  pipe(action, prop(__, actions))(act);

const generator = assoc(__, __, {});

const addFunction = action =>
  actions = merge(action, actions)

const addFunctions = map(addFunction);

const dispatch = action =>
  call(getFunction(action), 
       payload(action),
       res(action));

module.exports = {
  generator,
  addFunction,
  addFunctions,
  dispatch,
  extract: {
    action,
    payload,
    res,
  }
};
