const { evolve, curry, assoc, defaultTo,
	      pipe, append, isNil, ifElse, props, prop, find } = require('ramda');
const uuid = require('uuid/v4');

var store = {
  documents: {},
  relations: {}
};

const initialize = cb => {
  store = { documents: {}, relations: {} };
  cb(null, store);
}; 

const storePart = part => prop(part, store);

const modify = curry((field, fn, cb) => {
  const transformer = assoc(field, fn, {});

  store = evolve(transformer, store);

  cb(null, store);
});

const id = prop('id');

const findDoc = curry((id, cb) => cb(null, prop(id, storePart("documents"))));

const add = curry((doc, cb) => {
  const id = uuid();
  const persistable = assoc('id', id, doc);

  modify('documents', assoc(id, persistable), (err, res) => cb(null, persistable));
});

const relate = curry((doc1, doc2, cb) => {
  const evolveExisting = evolve(assoc(id(doc1), append(id(doc2)), {}));
  const assocNew = assoc(id(doc1), [id(doc2)]);
  
  modify('relations',
	       ifElse(pipe(prop(id(doc1)), isNil),
		            assocNew,
		            evolveExisting),
	       cb);
});

const children = curry((doc, cb) => {
  const rels = prop("relations", store);
  const docs = prop("documents", store);
  
  const docRelatedIds = defaultTo([], prop(id(doc), rels))
  
  const docRelations = props(docRelatedIds, docs);
  
  cb(null, docRelations);
});

const parents = curry((doc, cb) => {
  const rels = prop("relations", store);
  const docs = prop("documents", store);

  const asyncPredicate = async.asyncify(pipe(toPairs, last, contains(id(doc))));
  
  async.filter(rels, asyncPredicate, (err, paren) => {
    const docRelatedIds = defaultTo([], keys(paren))
    
    const docRelations = props(docRelatedIds, docs);
  
    cb(null, docRelations);
  });
});

module.exports = {
  initialize,
  add,
  find: findDoc,
  relate,
  children,
  parents
}
