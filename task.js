const cron = require('node-cron');
const { assoc, forEach } = require('ramda');

var tasks = {};

const addTask = (task) => {
  tasks[task.name] = cron.schedule(task.pattern, task.execute, false);
  return tasks[task.name];
}
const addTasks = forEach(addTask)

const startTask = (name) => tasks[name].start();
const startTasks = forEach(startTask);
const stopTask = (name) => tasks[name].stop();
const stopTasks = forEach(stopTask);
const destroyTask = (name) => tasks[name].destroy();
const destroyTasks = forEach(destroyTask);

const listTasks = () => tasks;
const findTask = (name) => tasks[name];

const generator = (name, pattern, execute) => ({ name, pattern, execute });

module.exports = {
  generator,
  addTask,
  addTasks,
  startTask,
  stopTask,
  destroyTask,
  startTasks,
  stopTasks,
  destroyTasks,
  listTasks,
  findTask
};
