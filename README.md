# Slurm
![Slurm](https://images.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.HQsTB5MgwvsAIGoLuRhWbgHaFW%26pid%3D15.1&f=1)

A highly addictive web framework to build applications.
It uses fp paterns and tries to make building apps easy.

## Slurm stands for?
- **S**imple
- **L**ightweight
- **U**niform
- **R**apid
- **M**odular

## What people say
> That damn curry is so addictive!

> Funtions. Functions everywhere!

> I'm so higher order!

## Technology
- hyperapp
- ramda
- express
- node-cron
- anydb
- axios
- browserify
- tachyons.css