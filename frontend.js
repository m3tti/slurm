const { curry, map, nth, is } = require('ramda');
const { h, app } = require('hyperapp');
const fx = require('@hyperapp/fx');

const harry = dom => {
  const type = nth(0, dom);
  const attrs = nth(1, dom);
  const value = nth(2, dom);
  
  if (is(String, value)) {
    return h(type, attrs, value);
  }
  
  return h(type, attrs, map(harry, value));
};

const callHarry = viewfn => (state, actions) => harry(viewfn(state, actions))

const frontend = {
  fx,
  app: curry((state, viewfn, actions) =>
             document.addEventListener("DOMContentLoaded", event => {
	             fx.withFx(app)(state, actions, callHarry(viewfn), document.body);
             })),
  dispatch: curry((actionName, payload, successFnName, errorFnName) => {
    const data = { action: actionName, payload };
    return fx.http("/dispatch",
            successFnName,
            {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(data),
              error: errorFnName
            });
  }),
}

module.exports = frontend;
