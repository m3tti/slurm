const { evolve, curry, assoc, defaultTo, concat, __, map, values, last,
	join, pipe, append, isNil, ifElse, props, prop, find, head } = require('ramda');
const anydb = require('any-db');
const db = anydb.createConnection('sqlite3://test.db');
const { sequence } = require('./utils');

const INITIALIZE_DOCS = "create table documents (id varchar(255), value varchar(255));";
const INITIALIZE_RELS = "create table relations (parent_id varchar(255), child_id varchar(255));";
const INITIALIZE_INDEX = "create table index (id varchar(255), name varchar(255));";
const INITIALIZE_INDEX_DOC = "create table index_doc (index_id varchar(255), doc_id varchar(255));";

const ADD_DOC = "insert into documents (id, value) values (?,?);";
const ADD_RELATION = "insert into relations (parent_id, child_id) values (?,?);";
const ADD_INDEX = "insert into index (id, name) values (?,?);";
const ADD_INDEX_DOC = "insert into index_doc (index_id, doc_id) values (?,?);";

const FIND_CHILDREN = "select * from relations where parent_id=?;";
const FIND_PARENTS = "select * from relations where child_id=?;";
const FIND_DOC_BY_ID = "select * from documents where id=?;";
const FIND_DOC_BY_INDEX = "select * from documents INNER JOIN index_doc ON documents.id = index_doc.doc_id INNER JOIN index ON index_doc.index_id = index.id";

const NO_STATEMENT_GIVEN = "NO_STATEMENT_GIVEN";

const uuid = require('uuid/v4');
const log = pipe(concat("[DB] "), console.log);

const id = prop('id');
const extractRows = cb => (err, res) => cb(err, prop("rows", res));

const whereInStatement = vals => "select * from documents where id in ('" + join("','", vals) + "');";

const query = curry((sql, cb) => {
  if (isNil(prop("statement", sql))) {
    log(NO_STATEMENT_GIVEN);
    return cb(NO_STATEMENT_GIVEN, null);
  }
  
  log("STATEMENT: " + sql.statement);    
  if (isNil(prop("data", sql))) {
    db.query(sql.statement, cb);
  } else {
    log("DATA: " + sql.data);
    db.query(sql.statement, sql.data, cb);
  }
});

const initialize = sequence(map(pipe(assoc("statement", __, {}),
				     query),
				[INITIALIZE_DOCS, INITIALIZE_RELS, INITIALIZE_INDEX, INITIALIZE_INDEX_DOC]));

const findDoc = curry((id, cb) => query({ statement: FIND_DOC_BY_ID, data: [id] }, extractRows(cb)));

const findByIndex = curry((name, cb) =>
                          query({ statement: FIND_BY_INDEX,
                                  data: [name] }, cb));

const add = curry((doc, cb) => {
  const uid = uuid();
  const persistable = assoc('id', uid, doc);

  const persist = query({ statement: ADD_DOC,
			  data: [uid, JSON.stringify(persistable)] });

  persist((err, res) => cb(err, persistable));
});

const relate = curry((doc1, doc2, cb) => {
  query({ statement: ADD_RELATION,
	  data: [id(doc1), id(doc2)] }, (err, res) => cb(err, [id(doc1), id(doc2)]));
});

const index = curry((name, doc, cb) =>.{
  const uid = uuid();

  const addIdx = query({ statement: ADD_INDEX,
                         data: [uid, name] });
  const relateIdx = query({ statement: ADD_INDEX_DOC,
                            data: [uid, id(doc)]});

  async.parallel([addIdx, relateIdx], cb);
});

const children = curry((doc, cb) => {
  const execute = query({ statement: FIND_CHILDREN,
			  data: id(doc) });

  execute(extractRows((err, rels) => {
    const ids = map(pipe(values, last), rels);

    query({ statement: whereInStatement(ids) },
	  extractRows(cb));
  }));
});

const parents = curry((doc, cb) => {
  const execute = query({ statement: FIND_PARENTS,
			  data: id(doc) });

  execute(extractRows((err, rels) => {
    const ids = map(pipe(values, head), rels);

    query({ statement: whereInStatement(ids) },
	  extractRows(cb));
  }));
});

module.exports = {
  initialize,
  add,
  find: findDoc,
  relate,
  index,
  children,
  parents
}
