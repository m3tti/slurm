const { prop, path, pipe, __, call, merge, assoc } = require('ramda');
const { dispatch, extract } = require('./dispatcher');
const ospath = require('path');
const db = require('./db');
const express = require('express');
const bodyParser = require('body-parser');

const log = str => console.log("CORE: " + str);

const extractBody = prop('body');

const app = express();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 

app.use('/', express.static(ospath.join(__dirname, 'base')));
app.use('/frontend', express.static('frontend'));

app.post('/dispatch', (req, res) => {
  const body = extractBody(req);
  const action = extract.action(body); 
  const payload = extract.payload(body);

  console.log("Body: " + JSON.stringify(body));
  const data = { action, payload, res: d => res.send(d) };

  console.log("dispatching " + data.action + " payload " + JSON.stringify(data.payload));
  
  dispatch(data);
});

const startServer = (port, cb) => 
  app.listen(port, cb);

module.exports = {
  startServer,
};
