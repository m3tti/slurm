# Slurm

## Backend
### Start the server
To start the slurm server you can use 
```javascript
slurm.core.startServer(port, cb);
```
where the callback is in the manner of `(err, res)`

### Dispatcher
The dispatcher function is used to connect the frontend with the
Backend. The backend defines `dispatcher` functions which can be
called via the `dispatch` function on the frontend. The `dispatcher`
functions has to be added befor starting the server.
The `dispatcher` function in the backend has the given form.

```javascript
const fn = slurm.dispatcher.generator;

const myfkt = fn(functionName, (payload, res) => {});

slurm.dispatcher.addFunctions([myfkt]);
```

- **functionName**: **[String]** a name how to call the function from the
  frontend
- **payload**: **[any]** a payload which is send from the frontend to the
  backend
- **res**: **Function** a function which needs a json as response
  value
  
### Tasks
Tasks are based on `node-cron`. In slurm you've a generator function 
to generate a task the function is defined as: 

```javascript
// shorten the generator function
const task = slurm.tasks.genarator;

// define task
const mytask = task(taskName,
                    pattern,
                    () => {});
// register tasks
slurm.tasks.addTasks([mytask]);

// start task
slurm.tasks.startTask(<taskName>);
```

- **taskName**: A simple string
- **pattern**: Pattern is a standard cron string in the manner of
```
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of week (0 - 6) (Sunday to Saturday;
# │ │ │ │ │                                       7 is also Sunday on some systems)
# │ │ │ │ │
# │ │ │ │ │
# * * * * *
```
- **function**: the function given to the task is a simple function
  without parameters.
  
## Frontend
The frontend is based on Hyperapp and uses a custom wrapper called
`Harry` a combination of HTML and Array. So you can simply combine 
arrays in the form of `[element, attributes, children/value]`.

### Getting started
To get your view running you have to use the `app` function.

```javascript
app(state, view, actions);
```

### State
State is a simple container which can be only modifid via actions.
A sample initial state looks like.
```javascript
const state = {
    products: [],
    attributes: []
};
```

### View
A view is difined in `Harry` Style. A component could look like.
```javascript
const h1HelloWorld = ["h1", {}, "Hello World"];
```

The attribute section can use all attributes which can be used in
react e.g. `onClick`, `className` or `onChange`

```javascript
const button = actions => 
    ["button", { onClick: () => actions.doSomething() }, "click"]
```

In this example we use a function to get the actions out of the main
view. The main view function is defined as follows.
```javascript
const main = (state, actions) => []
```
The array is the given harry notation. State and actions are filled by
the app function on state changes. That is based on hyperapp which
calls the main function everytime the state of the app changes.
State changes are only triggred by actions.

### Actions
Actions are the only way to modify state. The action functions you can
define have the following form.
```javascript
const actions = {
    myaction: value => oldState => modifiedState
}
```
- **modifiedState**: Needs to return a new json array with the new app
  state

Furthermore if you have asynchronous actions it is also possible to
get all actions into the action. This is done by.
```javascript
const actions = {
    myAsync: value => (oldState, actions) => {
        setTimeout(() => actions.doIt(1), 1000); 
    },
    doIt: value => oldState => ({ newVal: value })
}
```

### Dispatch
The frontend has a custom function called `dispatch` this function is
used to connect to the backend. The `dispatch` function has the
following form. 
```javascript
dispatch(functionName, payload, cb);
```
- **functionName**: Simple string has to match a function in the
  backend
- **payload**: The payload that has to be transported to the function
- **cb**: callback in the manner of `(err, res)`

## Example App
Find an example app at [slurm-example|https://gitlab.com/m3tti/slurm-example]
